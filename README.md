# xtrack-exciter-demo

This project demonstrates the [new Exciter beam element in Xtrack](https://github.com/Xsuite/Xtrack/pull/279) by [Philipp Niedermayer](https://github.com/eltos)

- `exciter-demo.ipynb` contains the demo notebook
- `ps_near_third_integer.json` is a Xtrack JSON line, converted from the [PS SlowEx scenario on acc-models-ps](https://gitlab.cern.ch/acc-models/acc-models-ps/-/tree/2022/scenarios/east/4_slow_extraction), with modified PFW strengths
- `demo.gif` is a phase-space animation of the demonstration simulation. The code to create this animation is provided in the Jupyter notebook, but takes ~15 mins to run.

<img src="demo.gif" width=400 height=400>
